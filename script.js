let forename = document.getElementById("forename");
let surname = document.getElementById("surname");
let birth_year = document.getElementById("birth-year");
let password_output = document.getElementById("generated-password");

// let symbols = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!”#$%&'()*+,-./:;<=>?@[]^_`{|}~";
let symbols = "0123456789!”#$%&'()*+,-./:;<=>?@[]^_`{|}~";


const generatePassword = () => {
  let new_password = forename.value + surname.value + birth_year.value;
  console.log(new_password);
  
  new_password = replaceCharacters(new_password);
  console.log(new_password);
  
  new_password += symbols.charAt(Math.floor(Math.random() * symbols.length));
  console.log(new_password);
  
  password_output.innerHTML = `Your password: <u>${new_password}</u>`;
}


const replaceCharacters = (string) => {
  let new_string = string;
  
  for (let i = 0; i < new_string.length; i++) {
    new_string = new_string.replace("a", "4");
    new_string = new_string.replace("A", "4");
    new_string = new_string.replace("e", "3");
    new_string = new_string.replace("E", "3");
    new_string = new_string.replace("i", "1");
    new_string = new_string.replace("I", "1");
    new_string = new_string.replace("s", "5");
    new_string = new_string.replace("S", "5");
    new_string = new_string.replace("o", "0");
    new_string = new_string.replace("O", "0");
  }
  
  return new_string;
}