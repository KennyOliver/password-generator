# password-generator

![CodeFactor](https://www.codefactor.io/repository/github/KennyOliver/password-generator/badge?style=for-the-badge)
![Latest SemVer](https://img.shields.io/github/v/tag/KennyOliver/password-generator?label=version&sort=semver&style=for-the-badge)
![Repo Size](https://img.shields.io/github/repo-size/KennyOliver/password-generator?style=for-the-badge)
![Total Lines](https://img.shields.io/tokei/lines/github/KennyOliver/password-generator?style=for-the-badge)

[![repl](https://replit.com/badge/github/KennyOliver/password-generator)](https://replit.com/@KennyOliver/password-generator)

**Generate passwords with input data**

[![GitHub Pages](https://img.shields.io/badge/See%20Demo-252525?style=for-the-badge&logo=safari&logoColor=white&link=https://kennyoliver.github.io/password-generator)](https://kennyoliver.github.io/password-generator)

---
Kenny Oliver ©2021
